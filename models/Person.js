const { Schema, model } = require("mongoose");

const PersonSchema = new Schema({
    firstName: String,
    lastName: String,
});

const PersonModel = model("Person", PersonSchema);

module.exports = PersonModel;
