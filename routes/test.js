const { Router } = require("express");
const router = Router();

router.get("/", (req, res) => {
    res.send("Default route within test router");
});

module.exports = router;
