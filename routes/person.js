const { Router } = require("express");
const router = Router();
const Person = require("../models/Person");

router.get("/", async (req, res) => {
    const doc = await Person.find({}).catch(() => res.send("Error occurred"));

    if (!res.headersSent) {
        res.json(doc);
    }
});

router.post("/", async (req, res) => {
    const {
        body: { firstName, lastName },
    } = req;

    const doc = await Person.create({
        firstName,
        lastName,
    }).catch(() => res.send("Error occurred"));

    if (!res.headersSent) {
        res.json(doc);
    }
});

module.exports = router;
