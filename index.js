const express = require("express");
const { connect } = require("mongoose");

connect("mongodb://localhost:27017/test").then(() =>
    console.log("MongoDB connection established")
);

const testRouter = require("./routes/test");
const personRouter = require("./routes/person");

const app = express();
app.use(express.json());

app.use("/test", testRouter);
app.use("/person", personRouter);

app.listen(process.env.port ?? 8080, () => console.log("Listening"));
